// name: AlisaError
// outputs: 1
// initialize: class AlisaError {\n    constructor(category, errorText) {\n        if (\n            typeof(category) !== 'string' ||\n            typeof(errorText) !== 'string'\n        ) {\n            throw "ArgumentError"  \n        }\n        this.category = category\n        this.errorText = errorText\n    }\n    \n    get errorMessage() {\n        return `Ошибка в ${this.category}\\n${this.errorText}`\n    }\n}\ncontext.set('class', AlisaError)\nglobal.set('AlisaError', AlisaError)
// finalize: // Code added here will be run when the\n// node is being stopped or re-deployed.\n
// info: 
const category = 'standart'
const entity = new (context.get('class'))(category, msg.payload)
msg.payload = entity.errorMessage
return msg;