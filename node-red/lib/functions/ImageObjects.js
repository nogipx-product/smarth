// name: ImageObjects
// outputs: 2
// initialize: class ImageObjects {\n    constructor(data) {\n        if (!Array.isArray(data)) {\n           throw 'ImageObjects input is not array'\n        }\n        this.data = data;\n    }\n    \n    objects(className, minScore = 0.5) {\n        return this.data.filter((e) => {\n            return e.className === className \n                && e.score >= minScore \n        })\n    }\n    \n    get persons() { \n        return this.objects('person')\n    }\n    \n    get all() { return this.data }\n}\ncontext.set('class', ImageObjects)
// finalize: // Code added here will be run when the\n// node is being stopped or re-deployed.\n
// info: 
try {
    const objects = new (context.get('class'))(msg.payload)

    msg.payload = objects.all
    throw "test error"

    return [msg, null]
    
} catch (e) {
    if (typeof(e) === 'string') {
        return [null, e]
    }
    throw 'Error message should be string'
}
